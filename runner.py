#!/usr/bin/python

import sys
import time

current = time.localtime()

print("Hello world. It is {}/{} {}:{}:{}\n".format(current.tm_mon,current.tm_mday,current.tm_hour,current.tm_min,current.tm_sec))