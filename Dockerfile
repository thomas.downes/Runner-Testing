FROM debian:testing

MAINTAINER Tom Downes <thomas.downes@ligo.org>

# non-interactive debian installation
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# install updates
RUN apt-get update
RUN apt-get --assume-yes install python

COPY runner.py /usr/local/bin/runner
RUN chmod 0755 /usr/local/bin/runner